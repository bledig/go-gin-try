#!/bin/bash

GO_IMAGE=golang:1.7
MAIN_DIR=bledig/gin-try


docker run --rm -v "$GOPATH":/go -w /go/src/$MAIN_DIR \
  $GO_IMAGE ./build.sh $1
