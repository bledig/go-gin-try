#!/bin/bash
set -e

# Laden der Packages
go get -d -v

gofmt

if [ "$1" == "--without-libc" ]; then
    go build -v -o main .
else
    # Kompilieren als statisches Binary inkl. libc für Linux
    CGO_ENABLED=0 GOOS=linux go build -v -a -installsuffix cgo -o main .
fi
