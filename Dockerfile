FROM scratch

# zoneinfo wurde erzeugt unter Ubuntu mit tar cfz zoneinfo.tar.gz /usr/share/zoneinfo
ADD zoneinfo.tar.gz /
# ca-certificates.crt stammen aus einem ubuntu 16.04 von /etc/ssl/certs/ca-certificates.crt
COPY ca-certificates.crt /etc/ssl/certs/

COPY main /
COPY assets /assets
COPY templates /templates

CMD ["/main"]
