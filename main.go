package main

import (
	"fmt"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"log"
	"time"
)

const url_weather_dresden="http://api.openweathermap.org/data/2.5/weather?q=Dresden,de&appid=627285309050528714165247feb8ec62&lang=de&units=metric"


type WetterRecord struct {
	Main map[string]interface{} `json:"main"`
	Wind map[string]interface{} `json:"wind"`
}

func getWeatherInfo() []byte {
	resp, err := http.Get(url_weather_dresden)
	if err != nil {
		log.Fatal("getWeatherInfo: ", err)
		return nil
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("getWeatherInfo: ", err)
		return nil
	}
	log.Println("getWeatherInfo: "+string(body))
	return body
}

func convToJson(jsonData []byte) WetterRecord {
	var wetter WetterRecord
	err := json.Unmarshal(jsonData, &wetter)
	if err != nil {
		log.Fatal("convToJson: ", err)
	}
	return wetter
}


// Anzeigen Index-Seite
func handleIndex(c *gin.Context) {
	wetterJson := getWeatherInfo()
	var wetter WetterRecord
	if wetterJson != nil {
		wetter = convToJson(wetterJson)
	}
	currenTime := time.Now()
	location, _ := time.LoadLocation("Europe/Berlin")
	formatedTimestmp := currenTime.In(location).Format("2.Januar 2006  15:04 Uhr")
	c.HTML(http.StatusOK,
		"index.html",
		gin.H{
			"user": "Bernd Ledig",
			"temp": wetter.Main["temp"],
			"humidity": wetter.Main["humidity"],
			"wind_speed": wetter.Wind["speed"],
			"wind_deg": wetter.Wind["deg"],
			"timestamp": formatedTimestmp,
		})
}



func main() {
	r := gin.Default()
	r.LoadHTMLGlob("templates/*.html")
	r.GET("/", handleIndex)
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong2",
		})
	})
	r.Static("/assets", "./assets")
	fmt.Println("Start web server ...")
	r.Run() // listen and serve on 0.0.0.0:8080
}
